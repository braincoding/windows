﻿using QuickStat.Model;
using QuickStat.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuickStat
{
    public partial class FormMain : Form
    {
        DbRepository<Person> dbpersons = new DbRepository<Person>();
        DbRepository<Site> dbsites = new DbRepository<Site>();
        DbRepository<PersonPageRank> dbranks = new DbRepository<PersonPageRank>();

        int dictionaryActive = 0;

        public FormMain()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            foreach (Person x in dbpersons.GetList())
                comboPersons.Items.Add(x.Name);
            foreach (Site x in dbsites.GetList())
                comboSites.Items.Add(x.Name);
            foreach (Person x in dbpersons.GetList())
                comboBoxDictionaryPerson.Items.Add(x.Name);
        }

        private void comboStatType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboStatType.SelectedIndex == 1)
            {
                labelDateStart.Visible = true;
                labelDateFinish.Visible = true;
                dateStart.Visible = true;
                dateFinish.Visible = true;
                labelPersonName.Visible = true;
                comboPersons.Visible = true;
            }
            else
            {
                labelDateFinish.Visible = false;
                labelDateStart.Visible = false;
                dateStart.Visible = false;
                dateFinish.Visible = false;
                labelPersonName.Visible = false;
                comboPersons.Visible = false;
            }
        }

        private void buttonResult_Click(object sender, EventArgs e)
        {
            
            GetData();
        }

        private void GetData()
        {
            gridStatistic.Rows.Clear();
            Site siteResult = new Site();
            foreach (Site site in dbsites.GetList())
                if (site.Name == comboSites.SelectedItem.ToString())
                    siteResult = site;
            List<DataViewModel> list = new List<DataViewModel>();            
            GridColumnInitialize();
            if (comboStatType.SelectedIndex == 0)
            {
                list.Clear();
                foreach (Page page in siteResult.Pages)
                {
                    foreach (PersonPageRank rank in dbranks.GetList())
                    {
                        if (page.Id == rank.Page.Id)
                        {
                            DataViewModel data = new DataViewModel();
                            if (list.Find(y => y.Name == rank.Person.Name) != null)
                                list.Find(y => y.Name == rank.Person.Name).Rank += rank.Rank;
                            else
                            {
                                data.Name = rank.Person.Name;
                                data.Rank = rank.Rank;
                                list.Add(data);
                            }
                        }
                    }
                }
                foreach (DataViewModel x in list)
                    gridStatistic.Rows.Add(new string[] { x.Name, x.Rank.ToString() });
                labelHeader.Text = comboStatType.Text + " по сайту " + comboSites.SelectedItem.ToString();
            }
            else
            {
                list.Clear();
                foreach (Page x in siteResult.Pages)
                {
                    DataViewModel data = new DataViewModel();
                    if (x.LastScanDate != null)
                        if(DateCheck((DateTime)x.LastScanDate))
                            foreach (PersonPageRank rank in x.PersonPageRanks)
                            {
                                if (rank.Person.Name == comboPersons.SelectedItem.ToString())
                                    if (list.Find(y => y.Name == x.LastScanDate.ToString()) != null)
                                        list.Find(y => y.Name == x.LastScanDate.ToString()).Rank += 1;
                                    else
                                    {
                                        data.Name = x.LastScanDate.ToString();
                                        data.Rank = 1;
                                        list.Add(data);
                                    }
                            }
                }
                foreach (DataViewModel x in list)
                    gridStatistic.Rows.Add(new string[] { x.Name, x.Rank.ToString() });
                labelHeader.Text = comboStatType.Text + " по сайту " + comboSites.SelectedItem.ToString() + " для личности " + comboPersons.SelectedItem.ToString();
            }
        }
        private bool DateCheck (DateTime date)
        {
            DateTime start = dateStart.Value;
            DateTime finish = dateFinish.Value;
            if (date <= start || date >= finish)
                return false;
            else
                return true;                      
        }
        private void GridColumnInitialize()
        {
            gridStatistic.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridStatistic.ColumnCount = 2;
            if(comboStatType.SelectedIndex == 0)
            {
                gridStatistic.Columns[0].Name = "Имя";
                gridStatistic.Columns[1].Name = "Количество упоминаний";
            }
            else
            {
                gridStatistic.Columns[0].Name = "Дата";
                gridStatistic.Columns[1].Name = "Количество новых страниц";
            }
        }

        private void LoadPersons()
        {
            DictionaryGridInitialize();
            foreach (Person person in dbpersons.GetList())
                gridStatistic.Rows.Add(person.Name);
        }

        private void DictionaryGridInitialize()
        {
            gridStatistic.Columns.Clear();
            gridStatistic.Rows.Clear();
            gridStatistic.ColumnCount = 1;
            gridStatistic.Columns[0].Name = "Наименование";
        }

        private void LoadKeywords(string name)
        {
            DictionaryGridInitialize();
            List<Person> persons = dbpersons.GetList();
            Person next = persons.Find(x => x.Name == name);
            List<Keyword> keywords = new List<Keyword>();
            if ( next != null)
                keywords = next.Keywords.ToList();
            if(keywords.Count != 0)
                foreach (Keyword keyword in keywords)
                    gridStatistic.Rows.Add(keyword.Name);
        }

        private void LoadSites()
        {
            DictionaryGridInitialize();
            foreach (Site site in dbsites.GetList())
                gridStatistic.Rows.Add(site.Name);
        }

        private void buttonPersons_Click(object sender, EventArgs e)
        {
            LoadPersons();
            labelHeader.Text = "Справочник \"Личности\" ";
            labelDictionaryPerson.Visible = false;
            comboBoxDictionaryPerson.Visible = false;
            dictionaryActive = (int)Dictionaries.Persons;
        }

        private void buttonKeyWords_Click(object sender, EventArgs e)
        {
            DictionaryGridInitialize();
            labelHeader.Text = "Справочник \"Ключевые слова\" ";
            labelDictionaryPerson.Visible = true;
            comboBoxDictionaryPerson.Visible = true;
            dictionaryActive = (int)Dictionaries.KeyWords;
        }

        private void buttonSites_Click(object sender, EventArgs e)
        {
            LoadSites();
            labelHeader.Text = "Справочник \"Сайты\" ";
            labelDictionaryPerson.Visible = false;
            comboBoxDictionaryPerson.Visible = false;
            dictionaryActive = (int)Dictionaries.Sites;
        }

        private void comboBoxDictionaryPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadKeywords(comboBoxDictionaryPerson.SelectedItem.ToString());
            string personName;
            if (comboBoxDictionaryPerson.SelectedItem!= null)
                 personName= comboBoxDictionaryPerson.SelectedItem.ToString();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {

        }
    }
}
