﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickStat.Model
{
    public class DataViewModel
    {

        public string Name { get; set; }

        public int? Rank { get; set; }
    }
}
