﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickStat.Model
{
    public class Keyword
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual Person Person { get; set; }
        public int PersonID { get; set; }
    }
}
