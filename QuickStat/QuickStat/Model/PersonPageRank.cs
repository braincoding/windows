﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickStat.Model
{
    public class PersonPageRank
    {
        public int Id { get; set; }

        public virtual Person Person { get; set; }

        public virtual Page Page { get; set; }

        public int PersonID;

        public int PageID;

        public int? Rank { get; set; }
    }
}
