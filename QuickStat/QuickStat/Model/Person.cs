﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickStat.Model
{
    public class Person
    {
        public int Id { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }
        public virtual ICollection<Keyword> Keywords { get; set; } = new List<Keyword>();
        public virtual ICollection<Page> Pages { get; set; } = new List<Page>();
    }
}
