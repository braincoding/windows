﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickStat.Model
{
    public class Page
    {
        public int Id { get; set; }
        public string URL { get; set; }
        public virtual Site Site { get; set; }
        public int SiteID { get; set; }
        public DateTime FoundDateTime { get; set; }
        public DateTime? LastScanDate { get; set; }
        public virtual ICollection<PersonPageRank> PersonPageRanks { get; set; } = new List<PersonPageRank>();

    }
}
