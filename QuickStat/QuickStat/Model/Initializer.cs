﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickStat.Model
{
    class Initializer: DropCreateDatabaseIfModelChanges<Context>
    {
        protected override void Seed(Context context)
        {
            Random rnd = new Random();
            // добавляем фамилии людей
            string[] names = new string[] { "Путин", "Медведьев", "Навальный" };
            foreach (string x in names)
                context.Persons.Add(new Person { Name = x });
            
            //добавляем сайты
            context.Sites.Add(new Site { Name = "lenta.ru" });
            context.Sites.Add(new Site { Name = "rbc.ru" });
            context.SaveChanges();
            // добавляем страницы сайтов 
            foreach (Site site in context.Sites)
            {
                for (int i = 0; i < 10; i++)
                {
                    Page page = new Page();
                    page.URL = "http://" + site.Name + i.ToString();
                    page.SiteID = site.Id;
                    site.Pages.Add(page);
                    page.FoundDateTime = new DateTime(2016, 1, 1);
                    context.Pages.Add(page);
                }
            }
            context.SaveChanges();
            //добавляем данные по статистике посещения страниц
            foreach (Person person in context.Persons)
            {
                Person personX = person;
                foreach (Page page in context.Pages)
                {
                    PersonPageRank personPageRunk = new PersonPageRank();
                    personPageRunk.Rank = rnd.Next(0, 10);
                    personPageRunk.Page = page;
                    personPageRunk.Page.LastScanDate = new DateTime(2016, rnd.Next(1,DateTime.Now.Month+1), rnd.Next(1,29));
                    //personPageRunk.PageID = page.Id;
                    personPageRunk.Person = personX;
                    //personPageRunk.PersonID = personX.Id;
                    context.PersonPageRanks.Add(personPageRunk);
                }
            }
            //добавляем ключевые слова
            foreach(Person person in context.Persons)
            {
                Keyword newKeyword = new Keyword { Name = person.Name };
                person.Keywords.Add(newKeyword);
                context.Keywords.Add(newKeyword);
            }
            //MySqlCommand command = new MySqlCommand("SET CHARSET utf8");
            context.SaveChanges();
        }
    }
}
