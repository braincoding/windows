﻿using QuickStat.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickStat.Repository
{
    class DbRepository<T> : IRepository<T>
        where T: class
    {
        private Context context;
        private DbSet<T> dbset;

        public DbRepository()
        {
            context = new Context();
            dbset = context.Set<T>();
        }

        public void Add(T entity)
        {
            dbset.Add(entity);
            context.SaveChanges();
        }

        public void Delete(T entity)
        {
            dbset.Remove(entity);
        }

        public void Edit(T entity)
        {
            dbset.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        public T GetByID(int ID)
        {
            return dbset.Find(ID);
        }

        public List<T> GetList()
        {
            return dbset.ToList<T>();
        }

        public void Save(T entity)
        {
            context.SaveChanges();
        }

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        // ~DbRepository() {
        //   // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
        //   Dispose(false);
        // }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
