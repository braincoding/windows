﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickStat.Repository
{
    interface IRepository<T> : IDisposable
        where T : class
    {
        List<T> GetList();

        T GetByID(int ID);

        void Add(T entity);

        void Edit(T entity);

        void Delete(T entity);

        void Save(T entity);
    }
}
