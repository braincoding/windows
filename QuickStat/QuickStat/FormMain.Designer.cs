﻿namespace QuickStat
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxStat = new System.Windows.Forms.GroupBox();
            this.buttonResult = new System.Windows.Forms.Button();
            this.comboStatType = new System.Windows.Forms.ComboBox();
            this.comboPersons = new System.Windows.Forms.ComboBox();
            this.labelPersonName = new System.Windows.Forms.Label();
            this.dateFinish = new System.Windows.Forms.DateTimePicker();
            this.labelSite = new System.Windows.Forms.Label();
            this.dateStart = new System.Windows.Forms.DateTimePicker();
            this.labelDateStart = new System.Windows.Forms.Label();
            this.labelDateFinish = new System.Windows.Forms.Label();
            this.labelStatTipe = new System.Windows.Forms.Label();
            this.comboSites = new System.Windows.Forms.ComboBox();
            this.groupBoxDictionaries = new System.Windows.Forms.GroupBox();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonKeyWords = new System.Windows.Forms.Button();
            this.buttonPersons = new System.Windows.Forms.Button();
            this.buttonSites = new System.Windows.Forms.Button();
            this.gridStatistic = new System.Windows.Forms.DataGridView();
            this.labelHeader = new System.Windows.Forms.Label();
            this.comboBoxDictionaryPerson = new System.Windows.Forms.ComboBox();
            this.labelDictionaryPerson = new System.Windows.Forms.Label();
            this.groupBoxStat.SuspendLayout();
            this.groupBoxDictionaries.SuspendLayout();
            this.groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridStatistic)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxStat
            // 
            this.groupBoxStat.Controls.Add(this.buttonResult);
            this.groupBoxStat.Controls.Add(this.comboStatType);
            this.groupBoxStat.Controls.Add(this.comboPersons);
            this.groupBoxStat.Controls.Add(this.labelPersonName);
            this.groupBoxStat.Controls.Add(this.dateFinish);
            this.groupBoxStat.Controls.Add(this.labelSite);
            this.groupBoxStat.Controls.Add(this.dateStart);
            this.groupBoxStat.Controls.Add(this.labelDateStart);
            this.groupBoxStat.Controls.Add(this.labelDateFinish);
            this.groupBoxStat.Controls.Add(this.labelStatTipe);
            this.groupBoxStat.Controls.Add(this.comboSites);
            this.groupBoxStat.Location = new System.Drawing.Point(1, 3);
            this.groupBoxStat.Name = "groupBoxStat";
            this.groupBoxStat.Size = new System.Drawing.Size(475, 242);
            this.groupBoxStat.TabIndex = 22;
            this.groupBoxStat.TabStop = false;
            this.groupBoxStat.Text = "Статистика";
            // 
            // buttonResult
            // 
            this.buttonResult.BackColor = System.Drawing.SystemColors.Control;
            this.buttonResult.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.buttonResult.Location = new System.Drawing.Point(133, 200);
            this.buttonResult.Name = "buttonResult";
            this.buttonResult.Size = new System.Drawing.Size(328, 34);
            this.buttonResult.TabIndex = 9;
            this.buttonResult.Text = "Применить";
            this.buttonResult.UseVisualStyleBackColor = false;
            this.buttonResult.Click += new System.EventHandler(this.buttonResult_Click);
            // 
            // comboStatType
            // 
            this.comboStatType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboStatType.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboStatType.FormattingEnabled = true;
            this.comboStatType.Items.AddRange(new object[] {
            "общая статистика",
            "ежедневная статистика"});
            this.comboStatType.Location = new System.Drawing.Point(133, 23);
            this.comboStatType.Margin = new System.Windows.Forms.Padding(2);
            this.comboStatType.Name = "comboStatType";
            this.comboStatType.Size = new System.Drawing.Size(328, 29);
            this.comboStatType.TabIndex = 1;
            this.comboStatType.SelectedIndexChanged += new System.EventHandler(this.comboStatType_SelectedIndexChanged);
            // 
            // comboPersons
            // 
            this.comboPersons.DisplayMember = "Id";
            this.comboPersons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPersons.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboPersons.FormattingEnabled = true;
            this.comboPersons.Location = new System.Drawing.Point(133, 107);
            this.comboPersons.Margin = new System.Windows.Forms.Padding(2);
            this.comboPersons.Name = "comboPersons";
            this.comboPersons.Size = new System.Drawing.Size(328, 29);
            this.comboPersons.TabIndex = 11;
            this.comboPersons.ValueMember = "Id";
            this.comboPersons.Visible = false;
            // 
            // labelPersonName
            // 
            this.labelPersonName.Font = new System.Drawing.Font("Tahoma", 8F);
            this.labelPersonName.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelPersonName.Location = new System.Drawing.Point(9, 107);
            this.labelPersonName.Name = "labelPersonName";
            this.labelPersonName.Size = new System.Drawing.Size(105, 34);
            this.labelPersonName.TabIndex = 10;
            this.labelPersonName.Text = "Личность";
            this.labelPersonName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelPersonName.Visible = false;
            // 
            // dateFinish
            // 
            this.dateFinish.Font = new System.Drawing.Font("Tahoma", 8F);
            this.dateFinish.Location = new System.Drawing.Point(317, 165);
            this.dateFinish.Name = "dateFinish";
            this.dateFinish.Size = new System.Drawing.Size(144, 24);
            this.dateFinish.TabIndex = 5;
            this.dateFinish.Visible = false;
            // 
            // labelSite
            // 
            this.labelSite.Font = new System.Drawing.Font("Tahoma", 8F);
            this.labelSite.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelSite.Location = new System.Drawing.Point(9, 66);
            this.labelSite.Name = "labelSite";
            this.labelSite.Size = new System.Drawing.Size(105, 34);
            this.labelSite.TabIndex = 7;
            this.labelSite.Text = "Сайт";
            this.labelSite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dateStart
            // 
            this.dateStart.Font = new System.Drawing.Font("Tahoma", 8F);
            this.dateStart.Location = new System.Drawing.Point(133, 167);
            this.dateStart.Name = "dateStart";
            this.dateStart.Size = new System.Drawing.Size(145, 24);
            this.dateStart.TabIndex = 3;
            this.dateStart.Visible = false;
            // 
            // labelDateStart
            // 
            this.labelDateStart.Font = new System.Drawing.Font("Tahoma", 8F);
            this.labelDateStart.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelDateStart.Location = new System.Drawing.Point(9, 165);
            this.labelDateStart.Name = "labelDateStart";
            this.labelDateStart.Size = new System.Drawing.Size(105, 34);
            this.labelDateStart.TabIndex = 4;
            this.labelDateStart.Text = "Период с";
            this.labelDateStart.Visible = false;
            // 
            // labelDateFinish
            // 
            this.labelDateFinish.Font = new System.Drawing.Font("Tahoma", 8F);
            this.labelDateFinish.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelDateFinish.Location = new System.Drawing.Point(284, 165);
            this.labelDateFinish.Name = "labelDateFinish";
            this.labelDateFinish.Size = new System.Drawing.Size(28, 34);
            this.labelDateFinish.TabIndex = 6;
            this.labelDateFinish.Text = "по";
            this.labelDateFinish.Visible = false;
            // 
            // labelStatTipe
            // 
            this.labelStatTipe.Font = new System.Drawing.Font("Tahoma", 8F);
            this.labelStatTipe.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelStatTipe.Location = new System.Drawing.Point(9, 23);
            this.labelStatTipe.Name = "labelStatTipe";
            this.labelStatTipe.Size = new System.Drawing.Size(125, 34);
            this.labelStatTipe.TabIndex = 2;
            this.labelStatTipe.Text = "Тип статистики";
            this.labelStatTipe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboSites
            // 
            this.comboSites.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSites.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboSites.FormattingEnabled = true;
            this.comboSites.Location = new System.Drawing.Point(133, 66);
            this.comboSites.Margin = new System.Windows.Forms.Padding(2);
            this.comboSites.Name = "comboSites";
            this.comboSites.Size = new System.Drawing.Size(328, 29);
            this.comboSites.TabIndex = 8;
            // 
            // groupBoxDictionaries
            // 
            this.groupBoxDictionaries.Controls.Add(this.groupBox);
            this.groupBoxDictionaries.Controls.Add(this.buttonKeyWords);
            this.groupBoxDictionaries.Controls.Add(this.buttonPersons);
            this.groupBoxDictionaries.Controls.Add(this.buttonSites);
            this.groupBoxDictionaries.Location = new System.Drawing.Point(3, 260);
            this.groupBoxDictionaries.Name = "groupBoxDictionaries";
            this.groupBoxDictionaries.Size = new System.Drawing.Size(475, 185);
            this.groupBoxDictionaries.TabIndex = 23;
            this.groupBoxDictionaries.TabStop = false;
            this.groupBoxDictionaries.Text = "Справочники";
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.buttonEdit);
            this.groupBox.Controls.Add(this.buttonAdd);
            this.groupBox.Controls.Add(this.buttonDelete);
            this.groupBox.Location = new System.Drawing.Point(9, 76);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(452, 75);
            this.groupBox.TabIndex = 18;
            this.groupBox.TabStop = false;
            // 
            // buttonEdit
            // 
            this.buttonEdit.BackColor = System.Drawing.SystemColors.Control;
            this.buttonEdit.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonEdit.Location = new System.Drawing.Point(156, 21);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(130, 34);
            this.buttonEdit.TabIndex = 11;
            this.buttonEdit.Text = "Редактировать";
            this.buttonEdit.UseVisualStyleBackColor = false;
            // 
            // buttonAdd
            // 
            this.buttonAdd.BackColor = System.Drawing.SystemColors.Control;
            this.buttonAdd.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAdd.Location = new System.Drawing.Point(13, 21);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(130, 34);
            this.buttonAdd.TabIndex = 10;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDelete.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDelete.Location = new System.Drawing.Point(299, 21);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(130, 34);
            this.buttonDelete.TabIndex = 9;
            this.buttonDelete.Text = "Удалить";
            this.buttonDelete.UseVisualStyleBackColor = false;
            // 
            // buttonKeyWords
            // 
            this.buttonKeyWords.BackColor = System.Drawing.SystemColors.Control;
            this.buttonKeyWords.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonKeyWords.Location = new System.Drawing.Point(164, 26);
            this.buttonKeyWords.Name = "buttonKeyWords";
            this.buttonKeyWords.Size = new System.Drawing.Size(140, 35);
            this.buttonKeyWords.TabIndex = 11;
            this.buttonKeyWords.Text = "Ключевые слова";
            this.buttonKeyWords.UseVisualStyleBackColor = false;
            this.buttonKeyWords.Click += new System.EventHandler(this.buttonKeyWords_Click);
            // 
            // buttonPersons
            // 
            this.buttonPersons.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPersons.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPersons.Location = new System.Drawing.Point(6, 26);
            this.buttonPersons.Name = "buttonPersons";
            this.buttonPersons.Size = new System.Drawing.Size(140, 35);
            this.buttonPersons.TabIndex = 10;
            this.buttonPersons.Text = "Личности";
            this.buttonPersons.UseVisualStyleBackColor = false;
            this.buttonPersons.Click += new System.EventHandler(this.buttonPersons_Click);
            // 
            // buttonSites
            // 
            this.buttonSites.BackColor = System.Drawing.SystemColors.Control;
            this.buttonSites.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSites.Location = new System.Drawing.Point(322, 26);
            this.buttonSites.Name = "buttonSites";
            this.buttonSites.Size = new System.Drawing.Size(140, 35);
            this.buttonSites.TabIndex = 9;
            this.buttonSites.Text = "Сайты";
            this.buttonSites.UseVisualStyleBackColor = false;
            this.buttonSites.Click += new System.EventHandler(this.buttonSites_Click);
            // 
            // gridStatistic
            // 
            this.gridStatistic.AllowUserToAddRows = false;
            this.gridStatistic.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridStatistic.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridStatistic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridStatistic.Location = new System.Drawing.Point(484, 52);
            this.gridStatistic.Name = "gridStatistic";
            this.gridStatistic.ReadOnly = true;
            this.gridStatistic.RowTemplate.Height = 28;
            this.gridStatistic.Size = new System.Drawing.Size(548, 380);
            this.gridStatistic.TabIndex = 24;
            // 
            // labelHeader
            // 
            this.labelHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHeader.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelHeader.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelHeader.Location = new System.Drawing.Point(484, 8);
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(541, 41);
            this.labelHeader.TabIndex = 25;
            this.labelHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxDictionaryPerson
            // 
            this.comboBoxDictionaryPerson.DisplayMember = "Id";
            this.comboBoxDictionaryPerson.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDictionaryPerson.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboBoxDictionaryPerson.FormattingEnabled = true;
            this.comboBoxDictionaryPerson.Location = new System.Drawing.Point(870, 14);
            this.comboBoxDictionaryPerson.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxDictionaryPerson.Name = "comboBoxDictionaryPerson";
            this.comboBoxDictionaryPerson.Size = new System.Drawing.Size(155, 29);
            this.comboBoxDictionaryPerson.TabIndex = 13;
            this.comboBoxDictionaryPerson.ValueMember = "Id";
            this.comboBoxDictionaryPerson.Visible = false;
            this.comboBoxDictionaryPerson.SelectedIndexChanged += new System.EventHandler(this.comboBoxDictionaryPerson_SelectedIndexChanged);
            // 
            // labelDictionaryPerson
            // 
            this.labelDictionaryPerson.Font = new System.Drawing.Font("Tahoma", 8F);
            this.labelDictionaryPerson.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelDictionaryPerson.Location = new System.Drawing.Point(784, 9);
            this.labelDictionaryPerson.Name = "labelDictionaryPerson";
            this.labelDictionaryPerson.Size = new System.Drawing.Size(81, 34);
            this.labelDictionaryPerson.TabIndex = 12;
            this.labelDictionaryPerson.Text = "Персона";
            this.labelDictionaryPerson.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelDictionaryPerson.Visible = false;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 434);
            this.Controls.Add(this.comboBoxDictionaryPerson);
            this.Controls.Add(this.gridStatistic);
            this.Controls.Add(this.labelDictionaryPerson);
            this.Controls.Add(this.labelHeader);
            this.Controls.Add(this.groupBoxDictionaries);
            this.Controls.Add(this.groupBoxStat);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QickStat";
            this.groupBoxStat.ResumeLayout(false);
            this.groupBoxDictionaries.ResumeLayout(false);
            this.groupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridStatistic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxStat;
        private System.Windows.Forms.Button buttonResult;
        private System.Windows.Forms.ComboBox comboStatType;
        private System.Windows.Forms.ComboBox comboPersons;
        private System.Windows.Forms.Label labelPersonName;
        private System.Windows.Forms.DateTimePicker dateFinish;
        private System.Windows.Forms.Label labelSite;
        private System.Windows.Forms.DateTimePicker dateStart;
        private System.Windows.Forms.Label labelDateStart;
        private System.Windows.Forms.Label labelDateFinish;
        private System.Windows.Forms.Label labelStatTipe;
        private System.Windows.Forms.ComboBox comboSites;
        private System.Windows.Forms.GroupBox groupBoxDictionaries;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonKeyWords;
        private System.Windows.Forms.Button buttonPersons;
        private System.Windows.Forms.Button buttonSites;
        private System.Windows.Forms.DataGridView gridStatistic;
        private System.Windows.Forms.Label labelHeader;
        private System.Windows.Forms.ComboBox comboBoxDictionaryPerson;
        private System.Windows.Forms.Label labelDictionaryPerson;
    }
}

